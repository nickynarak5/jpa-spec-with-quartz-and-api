package com.gitlab.johnjvester.jpaspec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaSpecWithQuartzAndApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(JpaSpecWithQuartzAndApiApplication.class, args);
    }
}
